from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

#Create Table Structure
class HotInfo(Base):
    __tablename__ = 'hot_info'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(500), nullable=False)
    author = Column(String(500), nullable=False)
    created_utc = Column(DateTime(), nullable=False)
    ups = Column(Integer, nullable=False)
    num_comments = Column(Integer, nullable=False)

