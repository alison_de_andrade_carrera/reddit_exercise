"""
This class has the two endpoints logics implemented here. The flask application just needs to call them.

Author: Alison Carrera

"""
import datetime, json
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker

from HotInfo import HotInfo, Base
from HotInfo_Serialize import HotInfoSchema

#MYSQL Login informations

USER = "root"
PASSWORD = "199510"
IP = "127.0.0.1:3306"
SCHEMA_NAME = "reddit_exercise"

class SearchUtils():
    def __init__(self):

        #Create Connection
        self.engine = create_engine("mysql+pymysql://{}:{}@{}/{}".format(USER,
                                   PASSWORD, IP, SCHEMA_NAME))

        Base.metadata.bind = self.engine
        self.DBSession = sessionmaker(bind=self.engine)

        #Create a session from the openned connection.
        self.session = self.DBSession()


    def search_1(self, initial_date, end_date, order):
        """

        This function represents the Endpoint 1.

        initial_date:string -> 2018-01-02
        end_date:string -> 2018-01-03
        order:int -> 0 (ups); 1 (comments)

        """

        initial_date = datetime.datetime.strptime(initial_date, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d')

        result = None

        if order == 0:
            result = self.session.query(HotInfo).filter(HotInfo.created_utc.between(initial_date, end_date)).order_by(desc(HotInfo.ups)).all()
            
        elif order == 1:
            
            result = self.session.query(HotInfo).filter(HotInfo.created_utc.between(initial_date, end_date)).order_by(desc(HotInfo.num_comments)).all()

        if len(result) > 0:
            
            hot_s = HotInfoSchema(many=True)
            json_s = hot_s.dump(result).data
            
            json_string = str(json.dumps({"result": json_s}))
        else:
            json_string = str(json.dumps("None"))
            
        return json_string
    
    def search_2(self, order):
        
        """

        This function represents the Endpoint 2.
        
        order:int -> 0 (ups); 1 (comments)

        """
        
        result = None

        if order == 0:
            result = self.session.query(HotInfo.author).order_by(desc(HotInfo.ups)).all()
        elif order == 1:
            result = self.session.query(HotInfo.author).order_by(desc(HotInfo.num_comments)).all()

        if len(result) > 0:
            json_string = str(json.dumps({'users': result}))
        else:
            json_string = str(json.dumps("None"))
            
        return json_string
    
    
