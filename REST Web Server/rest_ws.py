from SearchUtils import SearchUtils
from flask import Flask, request, Response
import json

app = Flask(__name__)

@app.route('/search_1', methods = ['GET'])
def search_1():
    try:
        if request.method == 'GET':
            date1 = request.args.get('start_date')
            date2 = request.args.get('end_date')
            order = request.args.get('order')
            
            sh = SearchUtils()
            
            response_ = sh.search_1(str(date1), str(date2), int(order))            
            
            response = Response(response= response_, status=200, mimetype='application/json')
            
            return response
        
    except Exception:
        
        response = Response(response= str(json.dumps("An error has ocurred. Contact the admin.")), status=200, mimetype='application/json')
        return response

@app.route('/search_2', methods = ['GET'])
def search_2():
    try:
        if request.method == 'GET':        
            order = request.args.get('order')
            
            sh = SearchUtils()
            
            response_ = sh.search_2(int(order))
            
            response = Response(response= response_, status=200, mimetype='application/json')
            
            return response
        
    except Exception:
        
        response = Response(response= str(json.dumps("An error has ocurred. Contact the admin.")), status=200, mimetype='application/json')
        return response

if __name__ == '__main__':
    app.run()
