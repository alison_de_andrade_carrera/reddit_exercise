from marshmallow import Schema, fields

class HotInfoSchema(Schema):
    title = fields.String()
    author = fields.String()
    created_utc = fields.DateTime()
    ups = fields.Integer()
    num_comments = fields.Integer()
    