"""

This code is to be used in the first use of this solution.
You just need to run it once.

Author: Alison Carrera

"""

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

#MYSQL Login informations

USER = "root"
PASSWORD = "199510"
IP = "127.0.0.1:3306"
SCHEMA_NAME = "reddit_exercise"

#Connect with the MYSQL Server
engine = create_engine("mysql+pymysql://{}:{}@{}".format(USER, PASSWORD, IP))

#Create the database schema
engine.execute(CreateSchema(SCHEMA_NAME)) 

#Once create the schema we need to connect with it.
engine = create_engine("mysql+pymysql://{}:{}@{}/{}".format(USER, PASSWORD, IP, SCHEMA_NAME))

Base = declarative_base()

#Create Table Structure
class HotInfo(Base):
    __tablename__ = 'hot_info'
    
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(500), nullable=False)
    author = Column(String(500), nullable=False)
    created_utc = Column(DateTime(), nullable=False)
    ups = Column(Integer, nullable=False)
    num_comments = Column(Integer, nullable=False)

#Execute in DB
Base.metadata.create_all(engine)