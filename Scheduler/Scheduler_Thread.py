"""
This class represents a Thread job which its function is to insert data from Reddit to a database.

Author: Alison Carrera

"""

from threading import Thread
import time, datetime, json
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
 
from HotInfo import HotInfo, Base

class Scheduler_Thread(Thread):
    def __init__(self, bd_connection, scheduler_hour, scheduler_minute):       
 
        Thread.__init__(self)
        #Create Connection
        self.engine = create_engine(bd_connection)
        self.scheduler_hour = scheduler_hour 
        self.scheduler_minute = scheduler_minute
        
        Base.metadata.bind = self.engine
        self.DBSession = sessionmaker(bind=self.engine)
        
        #Create a session from the openned connection.
        self.session = self.DBSession()
        
 
    def run(self):
        while True:
            
            try:
            
                #Get time from the current PC.
                time_datetime = datetime.datetime.now().time()
                
                hour = time_datetime.hour
                minute = time_datetime.minute
                
                #If hour and minute are equals, run the job. The hour and minute only repeat once per day.
                if hour == self.scheduler_hour and minute == self.scheduler_minute:
                    
                    print("Running Job...")
                    
                    #Get json from reddit
                    json_response = requests.get("https://api.reddit.com/r/artificial/hot", 
                                                 headers = {'User-agent': 'Reddit Exercise'})
                    
                    #Convert json to python object
                    json_object = json.loads(json_response.content)
                    
                    list_infos = []
                    
                    for obj in json_object['data']['children']:
                        #Create a HotInfo object for every recording.
                        hot_info = HotInfo(title=obj['data']['title'].rstrip().encode("UTF-8"), 
                                           author=obj['data']['author'].rstrip().encode("UTF-8"), 
                                           created_utc=datetime.datetime.utcfromtimestamp(obj['data']['created_utc']), 
                                           num_comments=obj['data']['num_comments'], ups=obj['data']['ups'])
                        
                        list_infos.append(hot_info)
                    
                    #Save the list of table-objects Hotinfo in DB.
                    self.session.bulk_save_objects(list_infos)
                    self.session.commit()
                        
                    print("Database upgraded.")
                    print("Job is running...")
                    # Wait the next minute more 10 seconds to return to the while loop.
                    time.sleep(70)
                
                time.sleep(1)
                
            except Exception as e:
                print(str(e))
            