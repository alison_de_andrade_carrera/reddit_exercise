from Scheduler_Thread import Scheduler_Thread

#MYSQL Login informations

USER = "root"
PASSWORD = "199510"
IP = "127.0.0.1:3306"
SCHEMA_NAME = "reddit_exercise"

#Hour and minute for execute job. In my computer it used 24h format.
HOUR_JOB = 1
MINUTE_JOB = 52

#Create the job thread.
schedule_thread = Scheduler_Thread("mysql+pymysql://{}:{}@{}/{}".format(USER,
                                   PASSWORD, IP, SCHEMA_NAME), HOUR_JOB, MINUTE_JOB)

print("Job is running...")

#Start Thread
schedule_thread.start()
#Wait until thread finish. Never. Only with ctrl+c
schedule_thread.join()